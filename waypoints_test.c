	/* program that simulates the behavior a single drone */
#include "main.h" 
#include "world_simulator_api.h"  /* to interact directly with the simulator , and get ports */
#include "math.h"
#include <unistd.h>
#include "thrust.h"

#include "pospayload.h"
#include "tx_thread.h"
#include "rx_thread.h"

#include "tx_frame.h"

#define MAXTHR 	60

/*static volatile double	Kp_h = 0.05 , 
						Ki_h = 0.0009 , 
						Kd_h = 0.045 , 
						Kp_v = 0.6 ; */
/*****
 * evil globals 
 * */
int state = DRONE_IDLE;
int reachedFirst = 0;
float tolerance;

int64_t t;
int wait_time = 1; 	

int frontReady = 0;
int backReady = 0;

int changeWP(int wpt, int s){
	if((wpt+1) == (s*s)){
		return 0;
	}
	else
		return wpt+1;
}
float fakeBernoulli(float distance, float alpha, float Radius){
	float PDR = exp( -log10(2) * pow( (distance / Radius ), alpha ) ) ;
	//printf("FAKE BERNOULLI PDR %f  DISTANCE %f  RADIUS %f  ALPHA %f \n",PDR,distance,Radius,alpha);
	if(PDR>1)
		return 1;
	return 1/PDR;
}

float rand11(void){
	double a = ((double)rand() / (double)RAND_MAX) * 2;
	//printf("Rand11 %f\n", a);
	return a-1;
}
float marsagliaPolar(void){
	float u = rand11();
	float v = rand11();
	float s = (u*u) + (v*v);
	
	while(s>=1){
		u = rand11();
		v = rand11();
		s = (u*u) + (v*v);
	}
	//printf("Marsaglia %f %f %f %f\n",u,s,(-2*log(s)), sqrt((-2*log(s))/s) );
	return u * sqrt((-2*log(s))/s);	
}
int main(int argc, char *argv[])
{
	
	wptsTarget = 0;
	signal(SIGINT, sigint_handler);

	time_t t;

	/* start by "connecting" to the simulator clock: readtime ,pause, resume , etc */
	if ( SimClock_init() == NOTOKAY )
	{
		printf("Main@\t\tclock failed to init\n") ;
		return NOTOKAY ;
	}
	
	/* setup thread attr for all 4 threads */
	size_t desired_stack_size = 40000 ; /* 40KB for stack */
	pthread_attr_t thread_attr ;
	pthread_attr_init( &thread_attr );
	pthread_attr_setstacksize( &thread_attr, desired_stack_size ) ;
	struct thread_info tinfo[MAXTHR]; /* struct to save arguments for all 4 threads */

	if ( argc < 2)
	{	 
		fprintf( stderr , "Usage: %s <myid>\n", argv[0] );
		return -1 ;
	}

	int my_id = (int)strtol( argv[1] , NULL, 0 ) ;
	printf("Main@ my id: #%" PRIu8 ".\n" , my_id );
	//int sender = (int)strtol( argv[2] , NULL, 0 ) ;
	//printf("Main@ Am I sender? %" PRIu8 ".\n" , sender );
//	if (argc == 4){
	//int front_id;// = (int)strtol( argv[2] , NULL, 0 );
	//int back_id;//  = (int)strtol( argv[3] , NULL, 0 ) ;

	fflush( stdout ) ;
	
	uint8_t idx=0 ; /*thread idx */


	/* thread to get incoming esensor readings */
	tinfo[idx].arg1 = my_id ;                         
	if ( pthread_create( &tinfo[idx].thread_id, &thread_attr, &thread_rx_esensor, &tinfo[idx] ) != 0 )
	{
		fprintf( stderr , "Main@ Error spawning 'thread_rx_esensor' thread.\n") ;
		fflush( stdout )  ;
		exit( 1 ) ;
	}


	/* thread to get incoming isensor readings */
	idx++;
	tinfo[idx].arg1 = my_id ;                         
	if ( pthread_create( &tinfo[idx].thread_id, &thread_attr, &thread_rx_isensor, &tinfo[idx] ) != 0 )
	{
		fprintf( stderr , "Main@ Error spawning 'thread_rx_isensor' thread.\n") ;
		fflush( stdout );
		exit( 1 ) ;
	}


	/* thread to send actuation */
	idx++;
	tinfo[idx].arg1 = my_id ;     
	if ( pthread_create( &tinfo[idx].thread_id, &thread_attr, &thread_tx_actuation, &tinfo[idx] ) != 0 )
	{
		fprintf( stderr , "SIM@ Error spawning 'thread_tx_actuation' thread.\n") ;
		fflush( stdout )  ;
		exit( 1 ) ;
	}
	

	/*thread to send actual position*/
	idx++;
	tinfo[idx].arg1 = my_id ;      
	if ( pthread_create( &tinfo[idx].thread_id, &thread_attr, &thread_tx_80211, &tinfo[idx] ) != 0 )
	{
		fprintf( stderr , "SIM@ Error spawning 'thread_tx_80211' thread.\n") ;
		fflush( stdout )  ;
		exit( 1 ) ;
	}

	/*thread to read incoming position*/
	idx++;
	tinfo[idx].arg1 = my_id ;     
	if ( pthread_create( &tinfo[idx].thread_id, &thread_attr, &thread_rx_80211, &tinfo[idx] ) != 0 )
	{
		fprintf( stderr , "SIM@ Error spawning 'thread_tx_80211' thread.\n") ;
		fflush( stdout )  ;
		exit( 1 ) ;
	}
	
	idx++;   
	tinfo[idx].arg1 = my_id ; 
	if ( pthread_create( &tinfo[idx].thread_id, &thread_attr, &thread_txframe, &tinfo[idx] ) != 0 )
	{
		fprintf( stderr , "SIM@ Error spawning 'thread_tx_80211' thread.\n") ;
		fflush( stdout )  ;
		exit( 1 ) ;
	}

	/* Destroy the thread attributes object, since it is no longer needed */
    if ( pthread_attr_destroy( &thread_attr ) != 0 )
		fprintf( stderr , "error destroying attr\n") ;
	
	
	sleep(3);
	
	

	esensor_t tmp_ereading ;
	isensor_t tmp_ireading ;
	float headingCurr;

	wpts_cart currentGPS;
	wpts_cart desiredGPS;
	
	wpts_cart origin;
	origin.east = 0;
	origin.north = 0;

	desiredGPS.east = 0;
	desiredGPS.north = 0;
	//tmp_actuation.thrust.Z=0;
	float tx, ty; /*  target coordinates are tx,ty  */
	int uavN;
	int uavL;
	int uavS;

	actuator_t tmp_actuation ; 
	tmp_actuation.thrust.X = 0;
	tmp_actuation.thrust.Y = 0;
	tmp_actuation.thrust.Z = 0;

	my_pid_t error_value;
	error_value.p_e = 0;
	error_value.p_n = 0;

	float transmissionMultiplier = 1;
	float addedTime = 1;



    char *fn = malloc(15*sizeof(char));
	sprintf(fn,"./logs/log_%d.txt",my_id);
	printf(fn);
	FILE *f = fopen(fn	, "w");


	pospayload_t ext;
	wpts_cart nextPosition;
	wpts_cart prevPosition;
	while (1)
	{
		
		error_value.p_e = 0;
		error_value.p_n = 0;
		tmp_ereading = esensor_getReading();
		tmp_ireading = isensor_getReading();
		
		currentGPS = getCurrentGPSHeading(tmp_ereading, tmp_ireading, &headingCurr);
		
		
		currentGPS.id = wptsTarget;

	
		ext.id    = my_id;
		ext.north = currentGPS.north;
		ext.east  = currentGPS.east;
		ext.alt   = 0;
		ext.wp    = wptsTarget;
		setTxPosition(ext);
		
		pospayload_t payloadFront = getPayload(front_id);

		if(payloadFront.id != -1){
			frontReady = 1;
			nextPosition = payloadToWpts(payloadFront);
			printf("[Next] North %d    East %d   Wpt %d\n",nextPosition.north,nextPosition.east,nextPosition.id);
		}
		else
			nextPosition.id = -1;

		pospayload_t payloadBack = getPayload(back_id);

		if(payloadBack.id != -1){
			backReady = 1;
			prevPosition = payloadToWpts(payloadBack);
			printf("[Previous] North %d    East %d   Wpt %d\n",prevPosition.north,prevPosition.east,prevPosition.id);
		}
		else
			prevPosition.id = -1;
			

		if(state == DRONE_IDLE){
			printf("Drone %d waiting for setup signal\n", my_id);
			if (checkSetup() == 1)
			{
				uavN = getN(); 
				uavL = getL();
				uavS = getS();
				
				/*int firstOrLast = 0;
				if(my_id == 1){
					firstOrLast = 1;
					printf("First Drone!\n");
				}
				else if(my_id == uavN){	
					firstOrLast = -1;
					printf("Last Drone!\n");
				}*/

				int front = (my_id +1 );
				if(front == (uavN+1))
					front = 1;
				setFrontID(front); 

				int back = (my_id -1);
				if(back == 0)
					back = uavN;
				setBackID(back);

				tolerance = uavS * 0.1;

				TSP_wpts_init(uavL, uavS);

				state = DRONE_SETUP;

				wptsTarget = (my_id-1) * floor((uavL*uavL)/uavN);

				desiredGPS.east = wpts_list[wptsTarget].east;
				desiredGPS.north= wpts_list[wptsTarget].north;
			}
		}
		else if(state == DRONE_SETUP){
			printf("Drone %d setting up. Desired [%d,%d]  tolerance %f \n", my_id,desiredGPS.north,desiredGPS.east,tolerance);
			float norm;
			vect_dist_to_waypt(desiredGPS, currentGPS, &norm);


			error_value.p_e = 0;
			error_value.p_n = 0;


			tmp_actuation = thrustAlloc(desiredGPS,
									currentGPS,
									headingCurr,
									&error_value,
									tmp_ereading,
									0); //Sem compensação 

			if(norm < tolerance){
				state = DRONE_SETUP_STOP;
			}

			srand((unsigned) time(&t));
		}
		else if(state == DRONE_SETUP_STOP){
			printf("Drone %d waiting for other drones\n", my_id);
			float norm;
			vect_dist_to_waypt(desiredGPS, currentGPS, &norm);

			/*tmp_actuation = thrustAlloc(desiredGPS,
									currentGPS,
									headingCurr,	
									&error_value,
									tmp_ereading,
									0); //Sem compensação */
			//tmp_actuation.thrust.Y = 0;
			//tmp_actuation.thrust.X = 0;
			if (abs((float)tmp_ereading.vel_north/1000) < 1)
				tmp_actuation.thrust.Y = 0;
			else
				tmp_actuation.thrust.Y = (tmp_ereading.vel_north)/1000 * 0.1;

			if (abs((float)tmp_ereading.vel_east/1000) < 1)
				tmp_actuation.thrust.X = 0;
			else
				tmp_actuation.thrust.X = (tmp_ereading.vel_east/1000) * 0.1;

			if(checkSendReady())
				setTxReady();

			if(checkGoAlgo()){
				printf("DRONE_GO\n ");
				state = DRONE_GO;	
				wptsTarget = changeWP(wptsTarget,uavL);
				printf("Go Target: %d\n",wptsTarget);
				desiredGPS.east = wpts_list[wptsTarget].east;
				desiredGPS.north= wpts_list[wptsTarget].north;
			}
		}
		else if(state == DRONE_GO){
			printf("GOGOGO\n");
			float norm;
			vect_dist_to_waypt(desiredGPS, currentGPS, &norm);
			
			if(norm < tolerance){
					t = SimClock_get();
					state = DRONE_STOP;

					float distOrigin;
					vect_dist_to_waypt(origin, currentGPS, &distOrigin);
					transmissionMultiplier = fakeBernoulli(distOrigin, 2, 1.0*3*uavS);

					addedTime = marsagliaPolar();
					while(addedTime < -wait_time)
						addedTime = marsagliaPolar();

					printf("-------Entering Stop mode----- CLOCK: %ld\n Bernoulli Multi %f Marsaglia %f\n",t,transmissionMultiplier,addedTime);
					setFrame(NULL);
			}
			

			error_value.p_e = 0;
			error_value.p_n = 0;
			
			float thrustComp = 0;
			thrustComp = formationCtrl( nextPosition,  currentGPS,  prevPosition, uavN, uavL, uavS);

			tmp_actuation = thrustAlloc(desiredGPS,
									currentGPS,
									headingCurr,
									&error_value,
									tmp_ereading,
									thrustComp); //Sem compensação 
		}
		else if(state == DRONE_STOP){
			printf("Stopping\n");
			float norm;

			vect_dist_to_waypt(desiredGPS, currentGPS, &norm);
	
				int asd = 0 ;
			
				if(!sendingFrame()){
				//if( (SimClock_get() - t) > ((transmissionMultiplier)* (wait_time+addedTime) * MILLION)){
				//if( (SimClock_get() - t) > ((wait_time) * MILLION)){
					printf("----------\nChange waypoint %f %d\n----------------------\n", norm,wptsTarget);
						
					wptsTarget = changeWP(wptsTarget,uavL);

					desiredGPS.east = wpts_list[wptsTarget].east;
					desiredGPS.north= wpts_list[wptsTarget].north;		

					state = DRONE_GO;
				}
				else{
					printf("DRONE STOP - Sending Frame \n");
					//printf("DRONE STOP - %ld\n", SimClock_get() - t);
				}
			//}
			
			/*if(change_wpt(norm) == 1){

				desiredGPS.east = wpts_list[wptsTarget].east;
				desiredGPS.north= wpts_list[wptsTarget].north;		

				printf("--------------------\n[WPT NUMBER] %d \n[Desired GPS] %d  %d \n[Current GPS] %d   %d\n-----------------------\n",
							wptsTarget,
							desiredGPS.north,desiredGPS.east,
							currentGPS.north,currentGPS.east);	
			}
			*/
			
			if (abs((float)tmp_ereading.vel_north/1000) < 1)
				tmp_actuation.thrust.Y = 0;
			else
				tmp_actuation.thrust.Y = (tmp_ereading.vel_north)/1000 * 0.1;

			if (abs((float)tmp_ereading.vel_east/1000) < 1)
				tmp_actuation.thrust.X = 0;
			else
				tmp_actuation.thrust.X = (tmp_ereading.vel_east/1000) * 0.1;
		}

		int wptsDiffFront = -1;
		int wptsDiffBack = -1;


				
		float dNext, dPrev;
		dNext_dPrev(nextPosition, currentGPS, prevPosition, &dNext, &dPrev, uavS, uavL);


		int64_t zzz = SimClock_get();
		char *fn = malloc(100*sizeof(char));
		fprintf(f,"%ld %f %f   %d %d %d   %d %d %d   %d %d %d\n",zzz,dNext,dPrev,
													currentGPS.id  ,currentGPS.north,currentGPS.east,
													nextPosition.id,nextPosition.north,nextPosition.east,
													prevPosition.id,prevPosition.north,prevPosition.east);
	
		/*
		float thrustComp = 0;
		
		thrustComp = formationCtrl(nextPosition, currentGPS, prevPosition, , backReady, frontReady, firstOrLast);


*/
		setActuation(tmp_actuation) ; /* "send" actuation thread my lastest desired actuation vector */
		microsleep(500000) ;
	}

	/* wait here until all 4 threads safely terminate */
	for (idx = 0 ; idx < 4 ; idx ++)
		pthread_join( tinfo[idx].thread_id , NULL );
	printf("**MAIN - END**\n");
	return OKAY ;
}



/* ctrl+c signals this: */
void sigint_handler(int sig)
{
  (void)(sig); // suppress warning 
  exit(1);
}


