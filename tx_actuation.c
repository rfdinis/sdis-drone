/* actuation tx */

#include "tx_actuation.h"
#include "world_simulator_api.h"  /* to interact directly with the simulator , and get ports */


#include <netinet/in.h> // udp packets
#include <ifaddrs.h> // udp packets
#include <arpa/inet.h> // udp packets
#include <sys/types.h> // recvfrom
#include <sys/socket.h> //recvfrom
#include <string.h> // memcpy
#include <pthread.h> // we have threads in the app
#include <stdio.h> // printf
#include <stdlib.h>


extern uint8_t pdr_list[5][5] ; /* defined in rx_80211,  estimation of pdr, matrix [src][dst] */

actuator_t actuation_vector;

void setActuation( actuator_t act_v )
{
	actuation_vector = act_v;
}


/* this thread sends actuation packets to the simulator*/
void * thread_tx_actuation(void *arg)
{
	struct thread_info tinfo= *(struct thread_info *)arg ;
	uint8_t my_id = (uint8_t)tinfo.arg1 ; 
	printf("ACTUATION@TID %" PRIu64 " Mission Thread to TX actuation packets\n" , (int64_t)tinfo.thread_id  );
	

	//UDP stuff
	int sock_fd ; /* socket */

	
	/* prepare UDP socket */
	if ( ( sock_fd = socket( PF_INET, SOCK_DGRAM, IPPROTO_UDP ) ) ==-1)
		fprintf(stderr , "ACTUATION@ error geting udp socket\n");
	struct sockaddr_in si_me;
	prepare_sockaddr( my_id,  UDP_PORT_BASE_SRC_ACT + my_id, &si_me );
	if ( bind( sock_fd , (struct sockaddr*)&si_me, sizeof(si_me)) == -1 ) //link si_me (port+address) to the socket
	{
		printf("ACTUATION@ error binding sokt\n" );
		return NULL ;
	}
	printf(
		"ACTUATION@ bind ok at %s:%hu\n", 
		inet_ntoa(si_me.sin_addr),
		ntohs(si_me.sin_port) );
	
	struct sockaddr_in 	si_send ; 
	prepare_sockaddr( my_id, SIM_RX_ALL, &si_send );
	//int ret ; /* sendto return */
	//TODO: use a connect. since we are sending to same dst all the time

	while (1) 
	{
		char cmd_parameter[220];
		snprintf(cmd_parameter, sizeof(cmd_parameter)-1,
			"AT*PCMD=%ld,%d,%d,%d,%d,%d\r",
			1234L,
			1, /* 1 - move, 0-hover*/
			*(int*)&(actuation_vector.thrust.Y),
			*(int*)&(actuation_vector.thrust.X),
			*(int*)&(actuation_vector.thrust.Z),
			*(int*)&(actuation_vector.torque) 
			) ;
		
		int ret = (int)sendto( sock_fd , cmd_parameter , strlen( cmd_parameter ) , 0 ,
						(const struct sockaddr *)&si_send , (socklen_t)sizeof(si_send) ) ;
		if ( ret < 0 )
		{
			printf("ACTUATION@[CLOCK %07luus] #%d - Error delivering actuation to simulator! * TERMINATING :S\n\n" , 
				SimClock_get() , (int)my_id ) ;
				return NULL ; 
		}
        microsleep( 1000000 ) ;    /* send this once a second +- */
	}
	return NULL ;
}

