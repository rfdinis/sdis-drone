#include "world_simulator_api.h"  /* to interact directly with the simulator , and get ports */
#include "generic.h"  
#include "p80211.h"
//include "tx_80211.h"
#include "pospayload.h"

#include <netinet/in.h> // udp packets
#include <ifaddrs.h> // udp packets
#include <arpa/inet.h> // udp packets
#include <sys/types.h> // recvfrom
#include <sys/socket.h> //recvfrom
#include <string.h> // memcpy
#include <pthread.h> // we have threads in the app
#include <stdio.h> // printf
#include <stdlib.h> // printf

#define MAX_PAYLOAD (220)

extern uint8_t pdr_list[5][5] ; /* defined in rx_80211,  estimation of pdr, matrix [src][dst] */
static p80211_simple_t pkt_g; /* the simplest packet we can send is an array of 300bytes*/
pthread_mutex_t payload_mut = PTHREAD_MUTEX_INITIALIZER; 

pospayload_t ereading = {0};
bool sendPos = false;
bool sendReady = false;
bool stopReady = false;

static bool sendFrame = false;
static uint16_t fplen = 0;

static p80211_simple_t eletro;

void sendFramePacket(uint8_t *data, uint16_t len)
{
	if(len > 300){printf("Frame packets are too large(>300)\n");exit(1);}
	//eletro.payload = malloc(sizeof(uint8_t) * len);
	eletro.seq_num = 666;
	memcpy(eletro.payload, data, len);
	fplen = len;
	sendFrame = true;
}

void setTxPosition(pospayload_t ext){
    ereading = ext;
    sendPos = true;
}

void setTxReady(void){
   
    sendReady = true;
}

void stopTxReady(){
	stopReady = true;
}

void * thread_tx_80211(void *arg)
{
	struct thread_info tinfo= *(struct thread_info *)arg ;
	
	int my_id    = tinfo.arg1 ; 

    while((front_id == -1)||(back_id == -1)){
		printf("Tx waiting\n");
		microsleep(1000000);
	}
	
	printf("TX80211@TID %"PRId64", ID: %d sendingo to: %d,%d\n" ,
		(int64_t)tinfo.thread_id ,my_id, front_id, back_id);
	
	//UDP stuff
	int txsock_fd ;

	/* prepare UDP socket */
	if ( ( txsock_fd = socket( PF_INET, SOCK_DGRAM, IPPROTO_UDP ) ) ==-1)
		fprintf(stderr , "TX80211@ error geting udp socket\n");
		
	/*bind source port */
	struct sockaddr_in	si_me ; // for bind socket
	si_me.sin_family		= AF_INET; //address format (ip4)
	si_me.sin_port			= htons( UDP_PORT_BASE_SRC_80211 + my_id ); //Simulator expects 802.11 pkts to come from this port
	si_me.sin_addr.s_addr	= htonl( INADDR_ANY ); // dntcare --- read pkts from anyone. this socket wont read so never
	if ( bind( txsock_fd , (SA)&si_me, SLEN ) == -1 ) //link si_me (port+address) to the socket
	{
		printf("TX80211@ error binding sokt\n" );
		return NULL ;
	}

	/* payload we will send is always the same */	
	uint16_t seq_num = 0 ;
	struct sockaddr_in si_front, si_back; // sockaddr for destination 
	struct sockaddr_in si_bs; //basestation sockaddr

	int enable = 1;
	setsockopt(txsock_fd,SOL_SOCKET,SO_REUSEPORT,&enable,sizeof(int));
	prepare_sockaddr( front_id, SIM_RX_ALL, &si_front);
	prepare_sockaddr( back_id, SIM_RX_ALL, &si_back);
	prepare_sockaddr( BSID, SIM_RX_ALL, &si_bs);
	
	printf("TX80211@STARTING TX %d--- Front %d    Back %d  \n" ,my_id,front_id,back_id) ;
	while (1) 
	{		
		if(sendPos)	{
			pkt_g.seq_num = ++seq_num;
			char * str = pptostr(ereading);
			if(str==NULL){
				printf("str null\n");
			}
			printf("Sending payload %20s \n",str);
			strcpy(pkt_g.payload,str);
			//printf("Sengin Front \n");
			//pthread_mutex_lock(&payload_mut);
			int ret = (int)sendto( txsock_fd, &pkt_g, sizeof(p80211_simple_t), 0,
						(const struct sockaddr *)&si_front, (socklen_t)sizeof(si_front) );

			//pthread_mutex_unlock(&payload_mut);
			//printf("Sent Front \n");
			if (ret < 0)
			{
				printf(
					"[CLOCK %07luus] Error delivering UDPpkts to %d:%s\n\n", 
					SimClock_get(),
					(int)ntohs( si_front.sin_port),
					inet_ntoa(si_front.sin_addr) );
				break;
			}

			pthread_mutex_lock(&payload_mut);
			ret = (int)sendto( txsock_fd, &pkt_g, sizeof(p80211_simple_t), 0,
					(const struct sockaddr *)&si_back, (socklen_t)sizeof(si_back) );
			pthread_mutex_unlock(&payload_mut);
			//printf("Sent Front \n");
			if (ret < 0)
			{
				printf(
					"[CLOCK %07luus] Error delivering UDPpkts to %d:%s\n\n", 
					SimClock_get(),
					(int)ntohs( si_back.sin_port),
					inet_ntoa(si_back.sin_addr) );
				break;
			}
			sendPos = 0;
		}
		
		if(sendReady){
			pkt_g.seq_num = 0;
			char * str = readypayload(my_id);
			if(str==NULL){
				printf("str null\n");
			}
			printf("Drone %d readu\n",my_id);
			strcpy(pkt_g.payload,str);
			//printf("Sengin Front \n");
			//pthread_mutex_lock(&payload_mut);
			int ret = (int)sendto( txsock_fd, &pkt_g, sizeof(p80211_simple_t), 0,
						(const struct sockaddr *)&si_bs, (socklen_t)sizeof(si_bs) );

			sendReady = 0;
		}
		if(stopReady){

			stopReady = 0;
		}

		if(sendFrame){
			//printf("Sending frame payload %c\n",eletro.payload[0]);
			
			sendto(txsock_fd,&eletro,fplen,0,(struct sockaddr*)&si_bs,sizeof(si_bs));
			sendFrame = false;
		}
        //microsleep(100000); /* 0.1s */ 
	}
	
	printf("[tx80211] thread is terminating\n");
	return NULL ;
}

