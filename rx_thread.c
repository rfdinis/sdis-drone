/* 80211 rx */
#include "world_simulator_api.h"  /* to interact directly with the simulator , and get ports */
#include "generic.h"  
#include "p80211.h"
#include "pospayload.h"

#include <netinet/in.h> // udp packets
#include <ifaddrs.h> // udp packets
#include <arpa/inet.h> // udp packets
#include <sys/types.h> // recvfrom
#include <sys/socket.h> //recvfrom
#include <string.h> // memcpy
#include <pthread.h> // we have threads in the app
#include <stdio.h> // printf
#include <stdlib.h> // atoi

#include "rx_thread.h"
#include "framecom.h"

/*delete?*/
uint16_t array_of_rcv_pkts[300];
uint8_t w_ptr = 0 ;
uint8_t pdr_list[5][5] ; /* estimation of pdr, pdr_list[src][dst] is the pdr at the link src->dst */ /*???????????????*/
/*\delete?*/


static pospayload_t pFront={0};
static pospayload_t pBack ={0};

static int16_t new_pFront = 0;
static int16_t new_pBack  = 0;

static uint16_t seq_pFront=0;
static uint16_t seq_pBack =0;

int N;
int L;
int S;
static uint8_t goSetup = 0;
int readyID; 
static uint8_t allowSendReady = 1;

#define RXACKBUFF_LEN 20
static bool rxFrameAck = false;
static uint8_t rxAckBuffer[RXACKBUFF_LEN] = {0};

uint8_t* get_frameAck(){
    if(!rxFrameAck) return NULL;
    uint8_t *out = (uint8_t *)malloc(RXACKBUFF_LEN*sizeof(uint8_t));
    memcpy(out,rxAckBuffer,RXACKBUFF_LEN);
    rxFrameAck = false;
    return out;
}

int16_t newRx(uint8_t id){
    if(id==pFront.id){
        return new_pFront;
    }
    else if(id==pBack.id){
        return new_pBack;
    }
    else
        return -1;
}
int hasFrame = 0;
int getHasFrame(void){
    return hasFrame;
}

framedata_t bs;
framedata_t getFrameData(void){
    hasFrame = 0;
    return bs;
}

pospayload_t getPayload(uint8_t id){
    pospayload_t empty;
    empty.id = -1;
    
    if(id==pFront.id){
        new_pFront=0;
        return pFront;
    }
    else if(id==pBack.id){
        new_pBack=0;
        return pBack;
    }
    else{
        printf("empty payload\n");
        return empty;
    }
        
}

int checkSetup(void){
    return goSetup;
}

int getN(void){
    return N;
}
int getL(void){
    return L;
}
int getS(void){
    return S;
}

int hasReady = 0;
int getIDready(void){

    if(hasReady){
        hasReady = 0;
        return readyID;
    }
    else
        return 0;
}


int checkSendReady(void){
    return allowSendReady;
}

static uint8_t goAlgo  = 0;
int checkGoAlgo(void){
    return goAlgo;
}

/* this thread receieve packets from other drones */
void * thread_rx_80211(void *arg)
{

	struct thread_info tinfo= *(struct thread_info *)arg ;
	uint8_t my_id    = (uint8_t)tinfo.arg1 ;
    //uint8_t front_id = (uint8_t)tinfo.arg2;
    //uint8_t back_id  = (uint8_t)tinfo.arg3;

	/* UDP stuff */
	int sock_fd ; // socket 
	ssize_t num_bytes_read ; //return for recvfrom()
	struct sockaddr_in 	si_other ; // sockaddr for the received packet
	socklen_t slen = (socklen_t)sizeof( si_other );


	if ( ( sock_fd = socket( PF_INET, SOCK_DGRAM, IPPROTO_UDP ) ) ==-1)
		printf( "RX80211@ error geting udp socket\n");
	
	/* bind lsitening ip and port */
	struct sockaddr_in		si_me ; // for bind socket
	prepare_sockaddr(my_id, PORT_UAV_RX80211 , &si_me);
	if ( bind( sock_fd , (struct sockaddr*)&si_me, sizeof(si_me)) == -1 ) //link si_me (port+address) to the socket
	{
		printf("RX80211@ error binding sokt\n" );
		return NULL ;
	}

	void *recv_data = malloc( MAX_80211_SIZE ) ; //allocate some bytes. ; // where received data will be. 
	if (NULL == recv_data)
	{
		printf("RX80211@ERROR %s:%d calloc()\n",__FUNCTION__,__LINE__);
		return NULL ;
	}
	

    printf("RX80211@STARTING RX %d \n" ,my_id) ;
	p80211_simple_t pkt;
	uint8_t  type , r_ptr ;

	while (1)
	{
        
		num_bytes_read = recvfrom( sock_fd, recv_data, MAX_80211_SIZE , 0, (struct sockaddr*)&si_other , &slen) ;
		memcpy( &pkt , recv_data , sizeof( p80211_simple_t ) ) ;
		
		//printf("RX80211@Got %dB, sqnum %d ---> payload %c \n" , (int)num_bytes_read , (int)pkt.seq_num,pkt.payload[0] ) ;

        if(pkt.payload[0]=='I'){
            pospayload_t temp = strtopp(pkt.payload);
            //sscanf(pkt.payload, "ID:%d N:%d E:%d A:%d",&temp.id,&temp.north,&temp.east,&temp.alt);
            //printf("ID:%d N:%d E:%d A:%d W:%d\n",temp.id,temp.north,temp.east,temp.alt,temp.wp);
            if(temp.id==back_id && pkt.seq_num > seq_pBack ){
                memcpy(&pBack,&temp,sizeof(temp));
                new_pBack++;
                seq_pBack = pkt.seq_num;
            }
            else if(temp.id==front_id && pkt.seq_num > seq_pFront ){
                memcpy(&pFront,&temp,sizeof(temp));
                new_pFront++;
                seq_pFront = pkt.seq_num;
            }
            else{
                printf("[RXTHREAD]WRONG PAYLOAD\n");
            }
        }
        else if(pkt.payload[0]=='S'){
            
            sscanf(pkt.payload, "Setup:%d L:%d S:%d", &N, &L, &S);
            printf("Received Start Packet %d %d %d\n", N, L, S);
            goSetup = 1;
        }
        else if(pkt.payload[0]=='R'){
            if(allowSendReady){
                hasReady = 1;
                sscanf(pkt.payload, "Ready:%d", &readyID);
                printf("Drone %d ready message\n", readyID);
            }
        }
        else if(pkt.payload[0]=='G'){
            printf("Stop sending ready %d \n",my_id);
            allowSendReady = 0;
        }
        else if(pkt.payload[0]=='L'){ //for frames
            framedata_t d;
            sscanf(pkt.payload, "L%02hhu%04hu%04u",&d.id,&d.seq_num,&d.data_len);
            //char *ip = inet_ntoa(si_other.sin_addr);
            //printf("Received frame data, seq=\n ID ID %02hhu\n",d.id);

            hasFrame = 1;
            bs = d;

        }
        else if(pkt.payload[0]=='A'){ //frame acknowledge
            //printf("Received ack: %s\n",pkt.payload);
            memcpy(rxAckBuffer,pkt.payload,20);
            rxFrameAck = true;
        }
        else if(pkt.payload[0]=='P'){
            printf("Start Sweep %d\n",my_id);
            goAlgo = 1;
        }
      
       // printf("RX80211@ %d Done, waiting for new payload.... \n", my_id);
	}
	return NULL ;
}
