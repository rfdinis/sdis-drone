/* interface with world simulator CLOCK */

//#include "world_simulator_api.h"  // get port numbers, read clock, etc


#include <stdio.h>	// printf
#include <inttypes.h>	// int16_t etc
#include <sys/mman.h>	/* shm_open */
#include <sys/stat.h>	/* For mode constants */
#include <fcntl.h>	/* For O_* constants */
#include <semaphore.h>	/* semaphore */
#include <unistd.h> /* close */

#include "generic.h"


/*************** 
 * evil globals
 * ************/
uint64_t * microseconds_ptr ; // shared mem space - 8 bytes to store "microseconds"
sem_t * clock_semaphore ;



/* "init" clock by opening a shared memory space that world simulator has already created */
int8_t SimClock_init()
{

	int oflag = O_RDONLY ; /* create . Read access only */
	mode_t mode = S_IRUSR ;
	int fd = shm_open( "clock_memspace"  , oflag  , mode );
	if (fd == -1)
	{
		printf("simclock@\tClock Memory Space does not exist :(. run ./drone_simulator \n");
		//perror("shm_open");
		return NOTOKAY ;
	}
	

	microseconds_ptr = (uint64_t *)mmap( NULL , sizeof( uint64_t ) , PROT_READ , MAP_SHARED , fd , 0 ) ;
	if ( microseconds_ptr == MAP_FAILED )
	{
		printf("simclock@ mmap failed!") ;
		return NOTOKAY ;
	}
	
	close ( fd ) ; /* not needed anymore */ 
	
	clock_semaphore = sem_open("clock_semaphore", O_CREAT, 0777, 0) ;
	if ( clock_semaphore == SEM_FAILED )
	{
		printf("simclock@ Clock Semaphore does not exist!\n");
		return NOTOKAY ;
	}


	return OKAY ;
}


/* return sim clock ( microseconds ) */
uint64_t SimClock_get()
{
	uint64_t tmp_clock = 0 ; 
	sem_wait( clock_semaphore );  
	tmp_clock = *microseconds_ptr ;
	sem_post( clock_semaphore ); 
	return tmp_clock ;
}

/* pause clock time */
void SimClock_pause()
{
	sem_wait( clock_semaphore );  
	printf("[CLOCK %" PRIu64 "us] Paused\n" , *microseconds_ptr ) ;
}

/* resume clock time */
void SimClock_resume()
{
	sem_post( clock_semaphore ); 
	printf("[CLOCK %" PRIu64 "us] Resumed\n" , *microseconds_ptr ) ;
}

/* printf clock time */
void SimClock_print(void)
{
		
	sem_wait( clock_semaphore );  
	printf("[CLOCK %" PRIu64 "us]" , *microseconds_ptr ) ;
	sem_post( clock_semaphore ); 
}
