#ifndef __TX_THREAD__
#define __TX_THREAD__

#include <sys/types.h>
#include "p80211.h"
#include "pospayload.h"

pospayload_t setTxPosition(pospayload_t ext);
void * thread_tx_80211(void *arg);
void setTxReady(void);
void stopTxReady(void);
uint8_t* get_frameAck(void);
void sendFramePacket(uint8_t* data, uint16_t len);

#endif
