#ifndef __POSPAYLOAD_H__
#define __POSPAYLOAD_H__

#include <sys/types.h>
#include "wpts.h"

#define BSID 9

typedef struct{
    int8_t id;
    int32_t north;
    int32_t east;
    int32_t alt;
    int32_t wp; /*last waypoint*/
}pospayload_t;

extern int front_id;
extern int back_id;

char* pptostr(pospayload_t p);
pospayload_t strtopp(char* str);
wpts_cart payloadToWpts(pospayload_t payload);
char *startpayload(int N, int L, int S);
char * readypayload(int ID);
char * gotreadypayload(int ID);
char * gopayload(int ID);


void setFrontID(int ID);
void setBackID(int ID);



#endif
