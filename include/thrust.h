#include "world_simulator_api.h"
#include <unistd.h>
#include <math.h>
#include "rx_esensor.h"
#include "rx_isensor.h"
#include "tx_actuation.h"
#include <stdio.h>
#include <stdlib.h>

#include "wpts.h"

#define MIN(a,b)	(((a)<(b))?(a):(b))
#define MAX(a,b)	(((a)<(b))?(b):(a))    
//#define BOUND(a,b)	( ( a > 0 ) ? MIN( a , b ) : MAX( a , -b ) )  
#define BOUND(a,L)	MAX(MIN( (a) , (L) ), -(L) )  

enum drone_states
{
	DRONE_IDLE,
	DRONE_SETUP,
	DRONE_SETUP_STOP,
	DRONE_GO,
	DRONE_STOP
};

/* PID constants */
static volatile double Kp_h = 0.002,
					   Ki_h = 0.000,//0.0001,
					   Kd_h = 0.001;//0.02;

typedef struct {
	double p_n ;
	double last_p_n ;
	double i_n ;
	double d_n ;

	double p_e ;
	double last_p_e;
	double i_e;
	double d_e ;
} my_pid_t; 


//0.025
//0.0001
//0.02


wpts_cart wpts_list[512];
int wptsTarget;

void TSP_wpts_init(int side, int step);
void dNext_dPrev(wpts_cart next, wpts_cart curr, wpts_cart prev, float *dNext, float *dPrev, int step,int lado);
float formationCtrl(wpts_cart next, wpts_cart curr, wpts_cart prev, int numDrones,int lado, int step);
double wrapTo360(double angle_in_degrees);

double degrees_to_radians(double degrees);


wpts_cart vect_dist_to_waypt(wpts_cart next_gps, wpts_cart current_gps, float *norm );

wpts_cart getCurrentGPSHeading(esensor_t tmp_ereading, isensor_t tmp_ireading, float* gpsHeading);


actuator_t thrustAlloc(wpts_cart desiredGPS, 
						wpts_cart currentGPS, 
						float gpsHeading,
						my_pid_t *error_value,
						esensor_t tmp_ereading,
						float thrustComp);

