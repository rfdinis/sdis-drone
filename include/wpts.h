#ifndef __WPTS_H__
#define __WPTS_H__

typedef struct{

	int id;
	int32_t north;
	int32_t east;
	int32_t alt;

} wpts_cart;

#endif