#ifndef __FRAMECOM_H__
#define __FRAMECOM_H__

#include <sys/types.h>
#include <stdint.h>
#include <netinet/in.h> // udp packets
#include <ifaddrs.h> // udp packets
#include <arpa/inet.h> // udp packets
#include <sys/types.h> // recvfrom
#include <sys/socket.h> //recvfrom
#include <string.h> // memcpy
#include <pthread.h> // we have threads in the app
#include <stdio.h> // printf
#include <stdlib.h> // printf

#define FRAME_ACK_LEN 20

/*
should be replaced with the simple packets...
*/
typedef struct{
    uint8_t id;
    uint16_t seq_num;
    uint32_t data_len;
    uint8_t* data; /*points to current addr in frame[], so we avoid 1 massive strcpys...*/

}framedata_t;

uint8_t* frame_getAck(framedata_t *d);
uint8_t valid_ack(uint8_t* buff, framedata_t* d);
uint8_t frame_sendPacket(framedata_t* d);
uint8_t frame_recvAck(framedata_t *d);

#endif
