/* return codes used across the sim */

#ifndef GENERIC_H
#define GENERIC_H


#include <inttypes.h> // int16_t etc
#include <pthread.h> // we have threads 
#include <netinet/in.h> // udp packets

#define CLEAR(x)	memset((void*)(&(x)), 0, sizeof(x))
#define	SA 		const struct sockaddr *
#define SLEN	(socklen_t)sizeof(struct sockaddr_in)

typedef uint8_t bool;
enum boolean {false, true};

typedef enum
{
	OKAY ,
	NOTOKAY
}
return_codes ;


struct thread_info {    /* Used as argument to thread_start() */
	pthread_t	thread_id;  /* ID returned by pthread_create() */
	int			arg1;       /* arg #1 */
	int			arg2;       /* arg #2 */
	int			arg3;       /* arg #3 */
};

typedef struct {
	float X ; /* 4 bytes */
	float Y ; /* 4 bytes */
	float Z ; /* 4 bytes */
} truple_t ; /* 12 bytes */


int microsleep( uint64_t tmp_us );


/** network related **/
void prepare_sockaddr(uint8_t dst, uint16_t port, struct sockaddr_in * addr);





#endif
