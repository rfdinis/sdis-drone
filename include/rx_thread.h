#ifndef __RX_THREAD__
#define __RX_THREAD__

#include <sys/types.h>
#include "p80211.h"
#include "pospayload.h"
#include "framecom.h"

pospayload_t getPayload(uint8_t id);
int16_t newRx(uint8_t id);
void * thread_rx_80211(void *arg);

int checkSetup(void);
int getN(void);
int getL(void);
int getS(void);

int getIDready(void);
int checkSendReady(void);
int checkGoAlgo(void);

int getHasFrame(void);
framedata_t getFrameData(void);

#endif
