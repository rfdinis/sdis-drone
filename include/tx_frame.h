#ifndef __TX_FRAME_H__
#define __TX_FRAME_H__

#include <stdint.h>

void setFrame(uint8_t *f);
void * thread_txframe(void* args);
uint8_t sendingFrame(void);

#endif