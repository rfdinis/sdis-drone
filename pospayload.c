#include "pospayload.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_LEN 220

int front_id = -1;
int back_id  = -1;

void setFrontID(int ID){
    front_id = ID;
}
void setBackID(int ID){
    back_id = ID;
}

char* pptostr(pospayload_t p){

    char *out = malloc(MAX_LEN*sizeof(char));
    if(out == NULL){
        return NULL;
    }
    sprintf(out,"ID:%hhd N:%d E:%d A:%d W:%d",p.id,p.north,p.east,p.alt,p.wp);
    return out;

}
pospayload_t strtopp(char* str){
    
    if(!str || strlen(str)<20) { return (pospayload_t){0};}
    pospayload_t p;
    sscanf(str, "ID:%hhd N:%d E:%d A:%d W:%d",&p.id,&p.north,&p.east,&p.alt,&p.wp);
    
    return p;
}

char * startpayload(int N, int L, int S){
    char *out = malloc(MAX_LEN*sizeof(char));
    if(out == NULL){
        return NULL;
    }
    sprintf(out,"Setup:%d L:%d S:%d",N,L,S);
    return out;
}

char * readypayload(int ID){
    char *out = malloc(MAX_LEN*sizeof(char));
    if(out == NULL){
        return NULL;
    }
    sprintf(out,"Ready:%d",ID);
    return out;
}

char * gotreadypayload(int ID){
    char *out = malloc(MAX_LEN*sizeof(char));
    if(out == NULL){
        return NULL;
    }
    sprintf(out,"GotReady:%d",ID);
    return out;
}
   
char * gopayload(int ID){
    char *out = malloc(MAX_LEN*sizeof(char));
    if(out == NULL){
        return NULL;
    }
    sprintf(out,"Potato:%d",ID);
    return out;
}
wpts_cart payloadToWpts(pospayload_t payload){
    wpts_cart zas;
    
    zas.id    = payload.wp;
    zas.north = payload.north;
    zas.east  = payload.east;
    zas.alt   = payload.alt;

    return zas;
}


int getFrontID(int ID){
    return ID;
}
int getBackID(int ID){
    return ID;
}
