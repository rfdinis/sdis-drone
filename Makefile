DRK_ROOT=../dronerk
LIBS=-lsimclock -pthread -lrt -lm

default: all

all: basestation drone

clean:
	rm uav_sim
	rm basestation

basestation:
	gcc -g  \
	-Iinclude/ \
	basestation.c generic.c tx_*.c rx_*.c pospayload.c framecom.c \
	thrust.c \
	-I$(DRK_ROOT)/include/ \
	-L$(DRK_ROOT)/lib/ \
	-lsimclock -L./libclock/ -pthread -lrt -lm -o basestation \
	-Wl,-rpath -Wl,./libclock/ \
	-Wl,-rpath -Wl,$(DRK_ROOT)/lib/

drone:
	gcc -g  \
	-Iinclude/ \
	waypoints_test.c generic.c tx_*.c rx_*.c pospayload.c framecom.c \
	thrust.c \
	-I$(DRK_ROOT)/include/ \
	-L$(DRK_ROOT)lib/ \
	-lsimclock -L./libclock/ -pthread -lrt -lm -o uav_sim \
	-Wl,-rpath -Wl,./libclock/ \
	-Wl,-rpath -Wl,$(DRK_ROOT)/lib/
