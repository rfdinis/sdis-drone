#include "tx_frame.h"
#include "framecom.h"
#include <unistd.h> //usleep
#include "generic.h"
#define FRAME_LEN (1000*1000)
#define NUM_PACKETS 5000 //creates 200 packets + 1 with the missing data if needed
#define PACKET_LEN (FRAME_LEN/NUM_PACKETS)
#define LAST_PACKET (FRAME_LEN%PACKET_LEN)

#define TIMEOUT_US (100000)

/*
TODO:
    PORT
    STATION IP
    Add a way to change the frame(that sets newFrame to 1)
    separate module for specified data structures(receive lens, formats, etc)
*/


static uint8_t *frame;
static uint8_t newFrame = 0;

enum state_e{
    START,
    SEND_PACKET,
    WAIT_ACK,
    NEXT_PACKET
};

uint8_t sendingFrame(){
    return newFrame;
}

void setFrame(uint8_t *f){
    frame = f;
    if(frame==NULL){
        frame = malloc(FRAME_LEN*sizeof(uint8_t)); //for testing purpposes
    }

    newFrame=1;
}

// returns 1 if there's more data to send
// returns 0 if over
int updateData(framedata_t* d,uint16_t seq_num){
    
    d->seq_num = seq_num;
    d->data_len = PACKET_LEN;
    d->data = &frame[seq_num*PACKET_LEN];
#if LAST_PACKET > 0
    if(seq_num==NUM_PACKETS){
        d->packet_size = LAST_PACKET;
        return 1;
    }
#endif
    return seq_num<NUM_PACKETS;
}





void * thread_txframe(void *arg){

    static enum state_e state = START;
    static uint16_t seq_num = 0;
    static framedata_t dat = {0};
    int res = 0; //aux variable
    	
    struct thread_info tinfo= *(struct thread_info *)arg ;
	dat.id    = (uint8_t)tinfo.arg1 ;
    //printf("TX FRAME ID %d\n",my_id);

    while(1){
        switch(state){
            case START:
                if(newFrame){
                    seq_num = 0;
                    state = NEXT_PACKET;
                    printf("[TXFRAME] Starting to send frame\n");
                }
                break;

            case NEXT_PACKET:
                newFrame = updateData(&dat,seq_num);
                state=SEND_PACKET; //unnecessary, but it is likely someone at some time will add a break to this case despite there being no need to

            case SEND_PACKET:
                if(dat.seq_num%500==0)
                    printf("[TXFRAME]sending seq %d, length %d\n",dat.seq_num,dat.data_len);
                frame_sendPacket(&dat);
                state = WAIT_ACK;
                break;

            case WAIT_ACK:
                //printf("[TXFRAME]waiting for acknowledge %d\n",seq_num);
                res=0;
                for(int i = 0; i<100 && !res ; i++)  //read with timeout
                {
                    res = frame_recvAck(&dat);
                    usleep(TIMEOUT_US/100);
                }
                //if timeout, resend frame
                if(!res){
                    printf("[TXFRAME]Timed out waiting for ACK %d, resending...\n",seq_num);
                    state = SEND_PACKET;
                }
                else{
                    seq_num++;
                    state = newFrame ? NEXT_PACKET : START;
                    //printf("[TXFRAME]Received correct ACK, proceding to packet %d\n",seq_num);
                }
                break;

            default:
                printf("[TXFRAME]I shouldn't be here... Back to start\n");
                state = START;
                return NULL;
        }
    }

    return NULL;
}
