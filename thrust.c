#include "world_simulator_api.h"
#include <unistd.h>
#include "thrust.h"


double wrapTo360( double angle_in_degrees )
{
	if( angle_in_degrees < 0 ) angle_in_degrees += 720;
	angle_in_degrees = fmod( angle_in_degrees , 360 ) ;
	return angle_in_degrees ; 
}

double degrees_to_radians( double degrees )
{
	return degrees * (M_PI/180.0);
}

wpts_cart vect_dist_to_waypt(wpts_cart next_gps, wpts_cart current_gps, float *norm )
{
	wpts_cart diff_gps;
	
	diff_gps.id = 0;
	diff_gps.north = next_gps.north - current_gps.north;
	diff_gps.east = next_gps.east - current_gps.east;
	diff_gps.alt = next_gps.alt - current_gps.alt;
	
	//printf("diff.north  %d  diff.east %d \n",diff_gps.north,diff_gps.east);
	int32_t northDiffSq = pow(abs(diff_gps.north), 2);
	int32_t eastDiffSq = pow(abs(diff_gps.east), 2);

	float zas = sqrt( northDiffSq + eastDiffSq);
	*norm = zas;
	//printf("[NORM] %f", *norm);
	return diff_gps;
}

wpts_cart getCurrentGPSHeading(esensor_t tmp_ereading, isensor_t tmp_ireading, float* gpsHeading){
	
	wpts_cart currentGPS;

	/*Read sensors*/
	float cmp = tmp_ireading.heading_degrees;

	*gpsHeading = cmp;

	currentGPS.north = tmp_ereading.north/1000;
	currentGPS.east = tmp_ereading.east/1000;
	//printf(" [Actual READ] X: %f  Y: %f \n [Head] %f ",currentGPS.north, currentGPS.east,cmp );

	return currentGPS;
}

void dNext_dPrev(wpts_cart next, wpts_cart curr, wpts_cart prev, float *dNext, float *dPrev, int step, int lado){

	//float *normCurr, *normPrev, *normNext;
	float normCurr, normPrev, normNext;
	//printf("CUUR  %d    \n",curr.id);
	//printf("WPTLIST %d    \n",wpts_list[curr.id].id);
	vect_dist_to_waypt(wpts_list[curr.id], curr, &normCurr);
	//printf("Current\n");
	//printf("Next  %d   \n ",next.id);
	//printf("WPTLIST %d    \n",wpts_list[next.id].id);
	
	//printf("Next\n");
	
	//printf("Prev\n");
	
	if(next.id != -1){
		vect_dist_to_waypt(wpts_list[next.id], next, &normNext);
		if (next.id > curr.id)
		{
			*dNext = ((float)(next.id - curr.id) * step) + (normCurr) - (normNext);
		}
		else{
			*dNext = (float)(((lado*lado) - (curr.id+1) + (next.id+1)) * step) + (normCurr) - (normNext);
		}
	}
	else{
		*dNext = -99999;
	}

	if(prev.id != -1){
		vect_dist_to_waypt(wpts_list[prev.id], prev, &normPrev);
		if(prev.id < curr.id){
			*dPrev = (float)((curr.id - prev.id) * step) - (normCurr) + (normPrev);
		}
		else{
			*dPrev = (float)(((lado*lado)  + (curr.id+1) - (prev.id+1)) * step) - (normCurr) + (normPrev);
		}
	}
	else{
		*dPrev = -99999;
	}
	

}

float formationCtrl(wpts_cart next, wpts_cart curr, wpts_cart prev, int numDrones, int lado, int step){
	/*firstOrLast
		-1  Last
		0   Middle
		1   First
	*/

	float dNext, dPrev;
	//printf("next %d %d %d --- curr %d %d %d ---prev %d %d %d",next.id,
	// if(!frontReady /*&& ((firstOrLast==-1)||(firstOrLast==0))*/ ){
	// 	printf("Front Drone not ready\n");
	// 	return 0;
	// }

	// if(!backReady /*&& ((firstOrLast==1)||(firstOrLast==0))*/){
	// 	printf("Back Drone not ready\n");
	// 	return 0;
	// }

	
	dNext_dPrev(next, curr, prev, &dNext, &dPrev, step, lado);
	
	if((dNext == -99999)  || (dPrev == -99999)){
		return 0;
	}
	else
		printf("dNExt %f  dPrev %f\n",dNext,dPrev);

	printf("First part algo done\n");
	
	if(dNext <= step){
		printf("Front slow\n");
		return 0;
	}
	else if(dPrev < step){
		printf("Im slow. Go fast\n");
		return 10;
	}
	else if(dPrev > (3*dNext)){
		
		if( (next.id+1) > ((lado*lado) / numDrones) )
			wptsTarget = (next.id+1) - floor((lado*lado) / numDrones);
		else
			wptsTarget = (lado*lado) + (next.id+1) - floor((lado*lado) / numDrones);
		printf("Go back. Target %d\n",wptsTarget);
		return -5;
	}
	else if(dPrev > dNext){
		printf("Slow down.\n");
		printf("Factor %f\n",(dPrev - dNext) / dPrev);
		return (dPrev - dNext) / (1+dPrev);
	}
	else{
		printf("Speed up.\n");
		printf("Speed up. Factor %f\n",(dNext - dPrev) / dNext);
		return (dNext - dPrev) / (dNext+1);
	}
}
actuator_t thrustAlloc(wpts_cart desiredGPS, 
						wpts_cart currentGPS, 
						float gpsHeading,
						my_pid_t *error_value,
						esensor_t tmp_ereading,
						float thrustComp){

	actuator_t thrustGo;
	float norm;
	float desiredHeading;
	float heading_diff;
	wpts_cart newCart;
	
	float magnitude_e, magnitude_n;
	
	int refresh_rate = 2.0;

	wpts_cart difference = vect_dist_to_waypt(desiredGPS, currentGPS, &norm);

	desiredHeading = atan2(difference.north,difference.east);
	desiredHeading = desiredHeading * (180.0 / M_PI);
	desiredHeading = wrapTo360(desiredHeading);

	//printf("Desired Heading %fº   GPS Heading %fº", desiredHeading, gpsHeading);
	heading_diff = desiredHeading - gpsHeading;

	if(heading_diff > 180){
		heading_diff -= 360;
	}

	if(heading_diff < 5 && heading_diff > -5)
		thrustGo.torque = 0;
	else
		thrustGo.torque = 0;
	/*Matriz rotacional WIP */

	//newCart.north = (difference.east * sin(degrees_to_radians(gpsHeading))) + (difference.north * cos(degrees_to_radians(gpsHeading)));
	//newCart.east = (difference.east * cos(degrees_to_radians(gpsHeading))) - (difference.north * sin(degrees_to_radians(gpsHeading)) );

	error_value->p_n = difference.north;
	error_value->p_e = difference.east;

	error_value->d_e = (error_value->p_e - error_value->last_p_e) *
					refresh_rate;
	error_value->d_n = (error_value->p_n - error_value->last_p_n) *
					refresh_rate;

	error_value->i_n += error_value->p_n 
						/ refresh_rate;
	error_value->i_e += error_value->p_e 
						/ refresh_rate;

	magnitude_e = Kp_h * error_value->p_e +
				Ki_h * error_value->i_e +
				Kd_h * error_value->d_e;

	magnitude_n = Kp_h * error_value->p_n +
				Ki_h * error_value->i_n +
				Kd_h * error_value->d_n;

	magnitude_e = BOUND(magnitude_e - (magnitude_e * thrustComp), 10);
	magnitude_n = BOUND(magnitude_n - (magnitude_n * thrustComp), 10);
	
	// magnitude_e = abs(magnitude_e);
	// magnitude_n = abs(magnitude_n);

	// if(magnitude_e > 0.1)
		// magnitude_e = 0.1;
			// 
	// if(magnitude_n > 0.1)
		// magnitude_n = 0.1;


	//thrustGo.thrust.X = 5;

	//thrustGo.thrust.X = -0.01 * newCart.east;
	//thrustGo.thrust.Y = -0.01 * newCart.north;
	thrustGo.thrust.X = -magnitude_e;// * difference.east;
	thrustGo.thrust.Y = -magnitude_n;// * difference.north;
	thrustGo.thrust.Z = 0;

/*
	if(state == DRONE_GO){
		thrustGo.thrust.X = -magnitude_e;// * difference.east;
		thrustGo.thrust.Y = -magnitude_n;// * difference.north;
		thrustGo.thrust.Z = 0;
	}
	else if( state == DRONE_STOP){
		printf("Vel North: %d Vel East %d\n", tmp_ereading.vel_north, tmp_ereading.vel_east);
		if (abs((float)tmp_ereading.vel_north/1000) < 1)
			thrustGo.thrust.Y = 0;
		else
			thrustGo.thrust.Y = (tmp_ereading.vel_north)/1000 * 0.1;

		if (abs((float)tmp_ereading.vel_east/1000) < 1)
			thrustGo.thrust.X = 0;
		else
			thrustGo.thrust.X = (tmp_ereading.vel_east/1000) * 0.1;
		
		thrustGo.thrust.Z = 0;
	}
*/

	error_value->last_p_e = error_value->p_e;
	error_value->last_p_n = error_value->p_n;
/*
	printf("\n\n-----------------------------------\n\n[PID INFO] -- MAG: %f   P: %f   I: %f   D: %f \n", 
				magnitude_e, error_value->p_e, error_value->i_e, error_value->d_e);
	printf("[PID INFO N] -- MAG: %f   P: %f   I: %f   D: %f \n------------------------------\n", 
				magnitude_n, error_value->p_n, error_value->i_n, error_value->d_n);
	*/
	
	
	//printf("[Desired] - N: %f  E: %f \n [Actual] N: %f  E: %f \n [Diff] X: %f  Y: %f \nHead: %f \n [NewCart] North %f East %f",
	//	   desiredGPS.north, desiredGPS.east,
	//	   currentGPS.north, currentGPS.east,
	//	   difference.north, difference.east,
	//	   heading_diff,
	//	   newCart.north, newCart.east);

	return thrustGo;
	

}

void TSP_wpts_init(int side, int step){
    
    int total_wpts = 0;

    /*up*/
    for (int i = 0; i < side; i++, total_wpts++)
    {
        wpts_list[total_wpts].id    = total_wpts;
		wpts_list[total_wpts].north = i * step;
		wpts_list[total_wpts].east  = 0;
        printf("ID %d   North: %d East: %d\n", wpts_list[total_wpts].id, wpts_list[total_wpts].north,wpts_list[total_wpts].east);
    }
    printf("up\n");
    /*right*/
    for (int i = 1; i < side ;i++,total_wpts++){
		wpts_list[total_wpts].id    = total_wpts;
		wpts_list[total_wpts].north = (side-1) * step;
		wpts_list[total_wpts].east  = i * step;
        printf("ID %d  North: %d East: %d\n", wpts_list[total_wpts].id, wpts_list[total_wpts].north,wpts_list[total_wpts].east);
	}
    printf("right\n");
    /*down*/
	for (int i = 1; i < side ;i++,total_wpts++){
		wpts_list[total_wpts].id    = total_wpts;
		wpts_list[total_wpts].north = ((side-1) * step) - (i * step);
		wpts_list[total_wpts].east  = (side-1) * step;
        printf("ID %d  North: %d East: %d\n", wpts_list[total_wpts].id, wpts_list[total_wpts].north,wpts_list[total_wpts].east);
	}
    printf("down\n");
    int numCurves = (side - 2) / 2;
    

    if(side % 2 == 0){
        
        for (int currentCurve = 0; currentCurve < numCurves; currentCurve++){
            /* 1st curve*/
            for (int i = 0; i < (side-1) ;i++,total_wpts++){
                wpts_list[total_wpts].id    = total_wpts;
                wpts_list[total_wpts].north = (i * step);
                wpts_list[total_wpts].east  = ((side-2-(2*currentCurve)) * step) ;
                printf("ID %d   North: %d East: %d\n", wpts_list[total_wpts].id, wpts_list[total_wpts].north,wpts_list[total_wpts].east);
            }
            printf("curve %d up \n",currentCurve);
            /* 2nd curve*/
            for (int i = 0; i < (side-1) ;i++,total_wpts++){
                wpts_list[total_wpts].id    = total_wpts;
                wpts_list[total_wpts].north = ((side-2) * step) - (i * step);
                wpts_list[total_wpts].east  = ((side-2-(2*currentCurve)-1) * step);
                printf("ID %d   North: %d East: %d\n",wpts_list[total_wpts].id, wpts_list[total_wpts].north,wpts_list[total_wpts].east);
            }
            printf("curve %d down \n",currentCurve);
        }
    }
    else{
        for (int currentCurve = 0; currentCurve < numCurves; currentCurve++){
             /* 1st curve*/
            for (int i = 0; i < (side-1) ;i++,total_wpts++){
                wpts_list[total_wpts].id    = total_wpts;
                wpts_list[total_wpts].north = (i * step);
                wpts_list[total_wpts].east  = ((side-2-(2*currentCurve)) * step) ;
                printf("ID %d   North: %d East: %d\n", wpts_list[total_wpts].id, wpts_list[total_wpts].north,wpts_list[total_wpts].east);
            }
            printf("curve %d up \n",currentCurve);
            
            if(currentCurve == numCurves -1)
                break;

            /* 2nd curve*/
            for (int i = 0; i < (side-1) ;i++,total_wpts++){
                wpts_list[total_wpts].id    = total_wpts;
                wpts_list[total_wpts].north = ((side-2) * step) - (i * step);
                wpts_list[total_wpts].east  = ((side-2-(2*currentCurve)-1) * step);
                printf("ID %d   North: %d East: %d\n",wpts_list[total_wpts].id, wpts_list[total_wpts].north,wpts_list[total_wpts].east);
            }
            printf("curve %d down \n",currentCurve);
        }
        wpts_list[total_wpts].id    = total_wpts;
        wpts_list[total_wpts].north = ((side-1) * step) - (step);
        wpts_list[total_wpts].east  = 2 * step;
        
        printf("ID %d   North: %d East: %d\n",wpts_list[total_wpts].id, wpts_list[total_wpts].north,wpts_list[total_wpts].east);
        total_wpts++;

        wpts_list[total_wpts].id    = total_wpts;
        wpts_list[total_wpts].north = ((side-1) * step) - (2 * step);
        wpts_list[total_wpts].east  = 2 * step;
        
        printf("ID %d   North: %d East: %d\n",wpts_list[total_wpts].id, wpts_list[total_wpts].north,wpts_list[total_wpts].east);
        total_wpts++;
        
        wpts_list[total_wpts].id    = total_wpts;
        wpts_list[total_wpts].north = ((side-1) * step) - (step);
        wpts_list[total_wpts].east  = step;
        
		printf("ID %d   North: %d East: %d\n",wpts_list[total_wpts].id, wpts_list[total_wpts].north,wpts_list[total_wpts].east);
        total_wpts++;

        wpts_list[total_wpts].id    = total_wpts;
        wpts_list[total_wpts].north = ((side-1) * step) - (2 * step);
        wpts_list[total_wpts].east  = step;
        
		printf("ID %d   North: %d East: %d\n",wpts_list[total_wpts].id, wpts_list[total_wpts].north,wpts_list[total_wpts].east);
        total_wpts++;
        int totalSqui = (side - 3) / 2;

        printf("squiggles\n");
        for (int i = 0; i < totalSqui;i++){
            
            wpts_list[total_wpts].id    = total_wpts;
            wpts_list[total_wpts].north = ((side -1) * step) - (3 * step) - (i * 2 * step);
            wpts_list[total_wpts].east  = step;
            
		printf("ID %d   North: %d East: %d\n",wpts_list[total_wpts].id, wpts_list[total_wpts].north,wpts_list[total_wpts].east);
            total_wpts++;

            wpts_list[total_wpts].id    = total_wpts;
            wpts_list[total_wpts].north = ((side -1) * step) - (3 * step) - (i * 2 * step);
            wpts_list[total_wpts].east  = 2 * step;
            
		printf("ID %d   North: %d East: %d\n",wpts_list[total_wpts].id, wpts_list[total_wpts].north,wpts_list[total_wpts].east);
            total_wpts++;
            wpts_list[total_wpts].id    = total_wpts;
            wpts_list[total_wpts].north = ((side -1) * step) - (4 * step) - (i * 2 * step);
            wpts_list[total_wpts].east  = 2 * step;
            
		printf("ID %d   North: %d East: %d\n",wpts_list[total_wpts].id, wpts_list[total_wpts].north,wpts_list[total_wpts].east);
           total_wpts++;
            wpts_list[total_wpts].id    = total_wpts;
            wpts_list[total_wpts].north = ((side -1) * step) - (4 * step) - (i * 2 * step);
            wpts_list[total_wpts].east  = step;
            
            printf("ID %d   North: %d East: %d\n",wpts_list[total_wpts].id, wpts_list[total_wpts].north,wpts_list[total_wpts].east);
            total_wpts++;
        }
    }
}