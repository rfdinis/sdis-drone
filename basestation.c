	/* program that simulates the behavior a single drone */
#include "main.h" 
#include "world_simulator_api.h"  /* to interact directly with the simulator , and get ports */
#include "math.h"
#include <unistd.h>
#include "thrust.h"

#include "pospayload.h"
#include "tx_thread.h"
#include "rx_thread.h"
#define MAXTHR 	6

int checkAllReady(int n, int *ready){

	for(int i = 1; i <= n; i++){
		if(ready[i] == 0)
			return 0;
	}
	return 1;
}

int main(int argc, char *argv[])
{
	
	signal(SIGINT, sigint_handler);

	/* start by "connecting" to the simulator clock: readtime ,pause, resume , etc */
	if ( SimClock_init() == NOTOKAY )
	{
		printf("Main@\t\tclock failed to init\n") ;
		return NOTOKAY ;
	}
	

	/* setup thread attr for all threads */
	size_t desired_stack_size = 35000 ; /* 40KB for stack */
	pthread_attr_t thread_attr ;
	pthread_attr_init( &thread_attr );
	pthread_attr_setstacksize( &thread_attr, desired_stack_size ) ;
	struct thread_info tinfo[MAXTHR] ; /* struct to save arguments for all 4 threads */

	if ( argc <3)
	{	 
		fprintf( stderr , "Usage: %s <myid> [<front_id> <back_id>]\n", argv[0] );
		return -1 ;
	}

	int N = (int)strtol( argv[1] , NULL, 0 );
	int L = (int)strtol( argv[2] , NULL, 0 );
	int S = (int)strtol( argv[3] , NULL, 0 );
	
	uint8_t idx=0;
	tinfo[idx].arg1 = BSID;  
	tinfo[idx].arg2 = BSID;  
	tinfo[idx].arg3 = BSID;                         
	if ( pthread_create( &tinfo[idx].thread_id, &thread_attr, &thread_rx_80211, &tinfo[idx] ) != 0 )
	{
		fprintf( stderr , "BaseStation@ Error spawning 'thread_rx_80211' thread.\n") ;
		fflush( stdout )  ;
		exit( 1 ) ;
	}

	/* Destroy the thread attributes object, since it is no longer needed */
    if ( pthread_attr_destroy( &thread_attr ) != 0 )
		fprintf( stderr , "error destroying attr\n") ;
	
	printf("----------------------------------------------\n");
	printf("----------------------------------------------\n");
	printf("-------------Starting BaseStation-------------\n");
	printf("----------------------------------------------\n");
	printf("----------------------------------------------\n");
	printf("-----------------%d Drones--------------------\n",N);
	printf("--------------%d x %d Waypoints---------------\n",L,L);
	printf("------------------%d Step---------------------\n",S);
	printf("----------------------------------------------\n");
	printf("----------------------------------------------\n");

	int batata = 1;
	char terminal[1000] = {0};
	sprintf(terminal,"gnome-terminal ");
	
	/*
	for (batata = 1; batata <= N; batata++){
		int zas = 1;
		char str[100] = {0};
		sprintf(str,"--tab --title=\"Drone %d\" -e \"./uav_sim %d\" ",batata,batata);
		//printf(" %-300s",str);
		strcat(terminal,str);
		
		//system(str);
		//printf("Terminal %d",batata);
		//microsleep(250000);
	}
	system(terminal);*/
	microsleep(1000000);
	/* RXTX Setup*/
	//-----------------------------------------------------
	//UDP stuff
	int txsock_fd;

	/* prepare UDP socket */
	if ( ( txsock_fd = socket( PF_INET, SOCK_DGRAM, IPPROTO_UDP ) ) ==-1)
		fprintf(stderr , "TX80211@ error geting udp socket\n");
		

	/*bind source port */
	struct sockaddr_in	si_me ; // for bind socket
	si_me.sin_family		= AF_INET; //address format (ip4)
	si_me.sin_port			= htons( UDP_PORT_BASE_SRC_80211 + BSID ); //Simulator expects 802.11 pkts to come from this port
	si_me.sin_addr.s_addr	= htonl( INADDR_ANY ); // dntcare --- read pkts from anyone. this socket wont read so never
	
	if (bind(txsock_fd, (SA)&si_me, SLEN) == -1) //link si_me (port+address) to the socket
	{
		printf("TX80211@ error binding sokt\n" );
		return -1;
	}
	int enable = 1;
	setsockopt(txsock_fd,SOL_SOCKET,SO_REUSEPORT,&enable,sizeof(int));
	//---------------------------------------------------
	p80211_simple_t pkt_g;
	pkt_g.seq_num = 0;
	/*Send start packet to drones*/
	
	struct sockaddr_in si_front[N+1];
	
	for(int j = 0; j < 20; j++){
		for (int i = 1; i <= N; i++){
		
								
			prepare_sockaddr( i, SIM_RX_ALL, &si_front[i]);
		
			char *msg = startpayload(N, L, S);
			strcpy(pkt_g.payload, msg);

			int ret = (int)sendto(txsock_fd, &pkt_g, sizeof(p80211_simple_t), 0,
									(const struct sockaddr *)&si_front[i], (socklen_t)sizeof(si_front[i]));

			printf("Sent %d bytes to %d  \n", ret, i);
			microsleep(5000);

		}
	}
	
	fflush( stdout ) ;

	int readyIDs[N+1];
	for(int i = 0; i < N; i++){
		readyIDs[i] = 0;
	}

	sleep(3);

	while( 1 )
	{
		int ID = getIDready();
		if(ID){
			readyIDs[ID] = 1;
			char *msg =  gotreadypayload(ID);

			strcpy(pkt_g.payload, msg);

			int ret = (int)sendto(txsock_fd, &pkt_g, sizeof(p80211_simple_t), 0,
								(const struct sockaddr *)&si_front[ID], (socklen_t)sizeof(si_front[ID]));
	
			
		}
		if(checkAllReady(N,readyIDs)){
			printf("All ready!\n");
			break;
		}
				
		
		microsleep(1000000) ;
	}
	
	for(int j = 0; j < 30; j++){
		for (int i = 1; i <= N; i++){	
		
			char *msg = gopayload(i);
			strcpy(pkt_g.payload, msg);

			int ret = (int)sendto(txsock_fd, &pkt_g, sizeof(p80211_simple_t), 0,
									(const struct sockaddr *)&si_front[i], (socklen_t)sizeof(si_front[i]));

			printf("Starting %d ID %d  \n", ret, i);
			microsleep(100);
		}
	}


	int fail = 1;
	int tryfails = 0; /*if enable, will avoid sending 50% of packets*/
	int framecount = 0;
	while( 1 )
	{

		if(getHasFrame()){
			framedata_t dodote;
			dodote = getFrameData();
			//printf("Sending ACK TO %d\n",dodote.id);
			char *msg = frame_getAck(&dodote);
			
			pkt_g.seq_num = 999;
			strcpy(pkt_g.payload, msg);

			fail ^= tryfails;
			putc(fail,stdout);
			if(fail){
				int nnn = sendto(txsock_fd,&pkt_g,sizeof(p80211_simple_t),0,(struct sockaddr*)&si_front[dodote.id],sizeof(si_front[dodote.id]));
				framecount++;
			}
			if(framecount % 1000 == 0)
			printf("framecount %d\n",framecount);
		}

	}
	

	/* wait here until all threads safely terminate */
	for (idx = 0 ; idx < 1 ; idx ++)
		pthread_join( tinfo[idx].thread_id , NULL );

	printf("**MAIN - END**\n");
	return OKAY ;
}



/* ctrl+c signals this: */
void sigint_handler(int sig)
{
  (void)(sig); // suppress warning 
  exit(1);
}


