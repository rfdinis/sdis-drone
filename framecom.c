#include "framecom.h"
#include "tx_thread.h"
#include "rx_thread.h"
#include <unistd.h>

#define FIRST_PACKET_LEN 20
#define PACKET_METADATA 11 /*L + 4digit seq num, + 4 digit datalen*/

uint8_t* frame_getAck(framedata_t *d){
    uint8_t *out= (uint8_t *)malloc(FRAME_ACK_LEN*sizeof(uint8_t));
    sprintf(out,"ACK%04hu",d->seq_num);
    return out;
}

uint8_t frame_sendPacket(framedata_t* d){

    uint8_t * packet = malloc(sizeof(uint8_t)*d->data_len+PACKET_METADATA);
    sprintf(packet,"L%02hhu%04hu%04u",d->id,d->seq_num,d->data_len);
    memcpy(packet+PACKET_METADATA,d->data,d->data_len);
    sendFramePacket(packet,d->data_len);
    return 0;
}


uint8_t valid_ack(uint8_t* buff, framedata_t* d){
    uint8_t *ans = frame_getAck(d);
    uint8_t equals = (strcmp(buff,ans)==0);
    free(ans);
    return equals;
}

uint8_t frame_recvAck(framedata_t *d){
    uint8_t *in;
    in = get_frameAck();
    if(in==NULL) return 0;
    uint8_t out = valid_ack(in,d);
    return out;
}